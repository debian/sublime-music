Screenshots
===========

All screenshots were taken using the `Arc Dark theme`_.

.. _Arc Dark theme: https://github.com/horst3180/Arc-theme

Album Cover Tab
---------------

.. image:: ./_static/screenshots/albums.png
   :target: ./_static/screenshots/albums.png

Artists Tab
-----------

.. image:: ./_static/screenshots/artists.png
   :target: ./_static/screenshots/artists.png

Browse Tab
----------

.. image:: ./_static/screenshots/browse.png
   :target: ./_static/screenshots/browse.png

Playlists Tab
-------------

.. image:: ./_static/screenshots/playlists.png
   :target: ./_static/screenshots/playlists.png

Play Queue
----------

.. image:: ./_static/screenshots/play-queue.png
   :target: ./_static/screenshots/play-queue.png

Play on Other Devices
---------------------

.. image:: ./_static/screenshots/chromecasts.png
   :target: ./_static/screenshots/chromecasts.png

Search
------

.. image:: ./_static/screenshots/search.png
   :target: ./_static/screenshots/search.png
